const api = require('../api')

const baseRota = 'categorias'

const novaForm = (req, res) => {
    res.render(baseRota + '/nova')
}

const nova = async (req, res) => {
    await api.create(baseRota, { titulo: req.body.titulo })
    res.redirect('/' + baseRota)
}

const list = async (req, res) => {
    const ret = await api.list(baseRota);
    res.render(baseRota + '/listar', { items: ret })
}

const excluir = async (req, res) => {
    await api.apagar(baseRota, req.params.id)
    res.redirect('/' + baseRota)
}

const editarForm = async (req, res) => {
    const itemAtual = await api.get(baseRota, req.params.id)
    res.render(baseRota + '/editar', { itemAtual })
}

const editar = async (req, res) => {
    await api.update(baseRota, req.params.id, { titulo: req.body.titulo })
    res.redirect('/' + baseRota)
}

module.exports = {
    novaForm, nova, list, excluir, editarForm, editar
}