const api = require('../api')

const baseRota = 'publicacoes'

const novaForm = async (req, res) => {
    const ret = await api.list('categorias');
    res.render(baseRota + '/nova', { items: ret })
}

const nova = async (req, res) => {
    await api.create(baseRota, { titulo: req.body.titulo, conteudo: req.body.conteudo, idcategoria: req.body.idcategoria })
    res.redirect('/' + baseRota)
}

const listPorCategoria = async (req, res) => {
    const idCategoria = req.params.id

    const ret = await api.list(baseRota);

    // Retorna todas as categorias
    const retCategorias = await api.list('categorias');

    const itemRecuperado = ret.filter(obj => {
        if (obj['idcategoria'] == idCategoria) {
            return obj
        }
    })

    let retCatPublicacao = []

    if (itemRecuperado.length > 0) {
        // Busca a categoria de cada registro
        retCatPublicacao = await getPromise(itemRecuperado)
    }

    res.render(baseRota + '/listar', { items: retCatPublicacao, itemsCategorias: retCategorias })
}

const list = async (req, res) => {
    const ret = await api.list(baseRota);

    // Retorna todas as categorias
    const retCategorias = await api.list('categorias');

    // Busca a categoria de cada registro
    const retCatPublicacao = await getPromise(ret)

    res.render(baseRota + '/listar', { items: retCatPublicacao, itemsCategorias: retCategorias })
}

const getPromise = (ret) => {
    const promises = ret.map(async (retAtual) => {
        return {
            ...retAtual,
            categoria: await api.get('categorias', retAtual.idcategoria)
        }
    });

    return Promise.all(promises);
}

const excluir = async (req, res) => {
    await api.apagar(baseRota, req.params.id)
    res.redirect('/' + baseRota)
}

const editarForm = async (req, res) => {
    const itemAtual = await api.get(baseRota, req.params.id)
    const ret = await api.list('categorias');
    res.render(baseRota + '/editar', { itemAtual, items: ret })
}

const editar = async (req, res) => {
    await api.update(baseRota, req.params.id, { titulo: req.body.titulo, conteudo: req.body.conteudo, idcategoria: req.body.idcategoria })
    res.redirect('/' + baseRota)
}

module.exports = {
    novaForm, nova, list, excluir, editarForm, editar, listPorCategoria
}