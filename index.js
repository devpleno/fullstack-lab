const axios = require('axios')
const bodyParser = require('body-parser')
const express = require('express')

const app = express()
app.set('view engine', 'ejs')
app.use(bodyParser.urlencoded())

const categorias = require('./routes/categorias')
app.use('/categorias', categorias)

const publicacoes = require('./routes/publicacoes')
app.use('/publicacoes', publicacoes)

const port = process.env.PORT || 3000

app.get('/', async (req, res) => {
    const baseURL = "https://fullstack-lab-68928.firebaseio.com"
    const conteudo = await axios.get(baseURL + '/teste.json')
    res.render('index', { i: conteudo.data })
})

app.listen(port, (error) => {
    if (error) {
        console.log('Ocorreu um erro', error)
    } else {
        console.log('Rodando na porta:' + port)
    }
})