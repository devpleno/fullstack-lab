const axios = require('axios')

const baseURL = "https://fullstack-lab-68928.firebaseio.com"
const auth = "HR01qMv82qsCYHs5Xia562K4MpeHazaeoo0r2pe7"

const list = async (key) => {
    const registros = await axios.get(baseURL + '/' + key + '.json?auth=' + auth)

    if (registros.data) {
        return Object.keys(registros.data).map(key => { return { id: key, ...registros.data[key] } })
    }

    return []
}

const apagar = async (key, id) => {
    await axios.delete(baseURL + '/' + key + '/' + id + '.json?auth=' + auth)
    return true;
}

const get = async (key, id) => {
    const registro = await axios.get(baseURL + '/' + key + '/' + id + '.json?auth=' + auth)

    return {
        id,
        ...registro.data
    }
}

const update = async (key, id, data) => {
    await axios.put(baseURL + '/' + key + '/' + id + '.json?auth=' + auth, data)
    return true;
}

const create = async (key, data) => {
    await axios.post(baseURL + '/' + key + '.json?auth=' + auth, data)
    return true;
}

module.exports = {
    create, list, update, apagar, get
}