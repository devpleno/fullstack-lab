const express = require('express')
const router = express.Router()

const controller = require('../controllers/categorias')

// Rota para listagem de categorias
router.get('', controller.list)

// Rota para o formulario de inserção
router.get('/nova', controller.novaForm)

// Recebe o POST do formulario
router.post('/nova', controller.nova)

// Rota para excluir
router.get('/excluir/:id', controller.excluir)

// Rota para o formulario de edição
router.get('/editar/:id', controller.editarForm)

// Recebe o POST do formulario de edição
router.post('/editar/:id', controller.editar)

module.exports = router